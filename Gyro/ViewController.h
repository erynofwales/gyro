//
//  ViewController.h
//  Gyro
//
//  Created by Eryn Wells on 10/3/14.
//  Copyright (c) 2014 Eryn Wells. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <CLLocationManagerDelegate>


@end

