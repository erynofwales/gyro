//
//  ViewController.m
//  Gyro
//
//  Created by Eryn Wells on 10/3/14.
//  Copyright (c) 2014 Eryn Wells. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <CoreMotion/CoreMotion.h>
#import "ViewController.h"


@interface ViewController ()

@property (weak) IBOutlet UILabel *headingLabel;
@property (weak) IBOutlet UILabel *rollLabel;
@property (weak) IBOutlet UILabel *pitchLabel;
@property (weak) IBOutlet UILabel *yawLabel;

@property (strong) CLLocationManager *locationManager;
@property (strong) CMMotionManager *motionManager;
@property (strong) NSOperationQueue *operationQueue;

@end


@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.operationQueue = [[NSOperationQueue alloc] init];
    [self initializeLocationData];
    [self initializeMotionData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - location data

- (void)initializeLocationData
{
    self.locationManager = [CLLocationManager new];
    self.locationManager.delegate = self;
    
    //self.locationManager.distanceFilter = 1000;
    //self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    //[self.locationManager startUpdatingLocation];
    
    if ([CLLocationManager headingAvailable]) {
        self.locationManager.headingFilter = 1;
        [self.locationManager startUpdatingHeading];
    }
}

- (BOOL)locationManagerShouldDisplayHeadingCalibration:(CLLocationManager *)manager
{
    return YES;
}

- (void)locationManager:(CLLocationManager *)manager
       didUpdateHeading:(CLHeading *)newHeading
{
    self.headingLabel.text = [NSString stringWithFormat:@"%.0fº", newHeading.trueHeading];
}

#pragma mark - motion data

- (void)initializeMotionData
{
    const double m180Pi = 180.0 * M_1_PI;
    
    self.motionManager = [CMMotionManager new];
    self.motionManager.deviceMotionUpdateInterval = 1.0 / 60.0;
    
    CMDeviceMotionHandler handler = ^(CMDeviceMotion *motion, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.rollLabel.text = [NSString stringWithFormat:@"%.0fº",
                                   motion.attitude.roll * m180Pi];
            self.pitchLabel.text = [NSString stringWithFormat:@"%.0fº",
                                    motion.attitude.pitch * m180Pi];
            self.yawLabel.text = [NSString stringWithFormat:@"%.0fº",
                                  motion.attitude.yaw * m180Pi];
        });
    };
    
    [self.motionManager startDeviceMotionUpdatesToQueue:self.operationQueue
                                            withHandler:handler];
}

@end
